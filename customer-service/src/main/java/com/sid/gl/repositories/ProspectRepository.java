package com.sid.gl.repositories;

import com.sid.gl.models.Prospect;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProspectRepository extends JpaRepository<Prospect,String> {
}
