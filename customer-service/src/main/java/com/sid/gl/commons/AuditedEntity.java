package com.sid.gl.commons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditedEntity<K> {
    @ReadOnlyProperty
    @CreationTimestamp
    @JsonIgnore
    protected LocalDateTime createdAt;

    @ReadOnlyProperty
    @UpdateTimestamp
    @JsonIgnore
    protected LocalDateTime updatedAt;

    @CreatedBy
    @JsonIgnore
    protected K createdBy;

    @LastModifiedBy
    @JsonIgnore
    protected K lastModifiedBy;
}
