package com.sid.gl.commons;


import jakarta.persistence.Id;

import java.util.Objects;


public abstract class BaseEntity<ID>  {
    @Id
    private ID id;

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        BaseEntity<?> that = (BaseEntity<?>) object;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
