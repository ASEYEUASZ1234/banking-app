package com.sid.gl.models;

import com.sid.gl.commons.AuditedEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import org.hibernate.annotations.GenericGenerator;
import java.util.Date;


@Entity
@Table(name="tb_prospect")
public class Prospect extends AuditedEntity<String> {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
   private String lastName;
   private String firstName;
   @Column(unique = true)
   private String username;
   @Email
   private String email;
   private String address;
   private String phoneNumber;
   @Enumerated(EnumType.STRING)
   private StatusProspect statusProspect= StatusProspect.PENDING;

   private Date dateBirthDay;

   private String cityBirhDay;

    public String getCityBirhDay() {
        return cityBirhDay;
    }

    public void setCityBirhDay(String cityBirhDay) {
        this.cityBirhDay = cityBirhDay;
    }

    public Date getDateBirthDay() {
        return dateBirthDay;
    }

    public void setDateBirthDay(Date dateBirthDay) {
        this.dateBirthDay = dateBirthDay;
    }

    public String getId() {
        return id;
    }

    public Prospect() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public StatusProspect getStatusProspect() {
        return statusProspect;
    }

    public void setStatusProspect(StatusProspect statusProspect) {
        this.statusProspect = statusProspect;
    }
}
