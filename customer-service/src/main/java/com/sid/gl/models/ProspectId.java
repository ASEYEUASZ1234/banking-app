package com.sid.gl.models;

import com.sid.gl.commons.BaseID;

import java.util.UUID;

public class ProspectId extends BaseID<UUID> {
    protected ProspectId(UUID value){
        super(value);
    }
}
