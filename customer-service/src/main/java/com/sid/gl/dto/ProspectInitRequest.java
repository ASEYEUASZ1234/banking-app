package com.sid.gl.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProspectInitRequest {
    private String id;
    @NotNull
    @Size(max = 50)
    private String lastName;
    @NotNull
    @Size(max = 100)
    private String firstName;
    @Email
    private String email;
    @Size(max = 10)
    private String phoneNumber;
    @Size(max = 120)
    private String address;
    private Date dateBirthday;
    @Size(max = 120)
    private String cityBirthDay;
}
