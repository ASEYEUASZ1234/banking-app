package com.sid.gl.dto;

import lombok.Data;

@Data
public class ProspectResponseTmp {
    private String id;
    private String fullName;
    private String username;
    private String email;
    private String phoneNumber;
    private String statusProspect;
}
