package com.sid.gl.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientResponse {
    private String username;
    private String correlationId;
    private String lastName;
    private String firstName;
    private String phoneNumber;
    private String email;

}
